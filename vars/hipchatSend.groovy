def call(Map args) {
  def colorMap = [
    'GREEN': '#00FF00',
    'RED': '#FF0000'
  ]

  mattermostSend(
    message: args.message.replaceAll(/<a href="(.*?)">(.*?)<\/a>/, '[$2]($1)'),
    color: colorMap[args.color]
  )
}
